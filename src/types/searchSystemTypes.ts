/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Component} from '@oliveai/ldk/dist/whisper';
import {WhisperComponent} from '@rotera.ai/base-ldk-framework/dist/utils';

export type SearchSystem = {
  doSearch: (query: string) => Promise<(WhisperComponent | Component)[]>;
};

/**
 * Search Provider Type
 * @property {string} name The name for a search provider, shown on search result whisper
 * @property {function} getComponentsForResults function
 *  that returns a list of components to display for a given query
 * @property {function} onOnlyResult optional function that will be called if
 *  provider is the only one with results. Implementers can handle and present
 *  a custom whisper or return false to show the default search result whisper.
 *  Please note: Currently providers must do a second search in this situation
 */
export type SearchProvider = {
  name: string;
  getComponentsForResults: (query: string) => (WhisperComponent | Component)[];
  onOnlyResult?: (query: string) => Promise<unknown | boolean>;
};
