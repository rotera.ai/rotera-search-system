/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {Component, WhisperComponentType} from '@oliveai/ldk/dist/whisper';
import {flatMap} from 'lodash';
import {SearchProvider, SearchSystem} from './types/searchSystemTypes';
import {WhisperComponent} from '@rotera.ai/base-ldk-framework/dist/utils';

/**
 * Create a search system
 * @param searchProviders a list of providers which can be used to generate search results
 */
export function createSearchSystem(
  searchProviders: SearchProvider[]
): SearchSystem {
  /**
   * Searches search providers for query
   * @param query string to search
   */
  async function doSearch(
    query: string
  ): Promise<(WhisperComponent | Component)[]> {
    const results = searchProviders
      .map(provider => {
        try {
          // Get results from the provider
          const results = provider.getComponentsForResults(query);
          return {
            provider: provider,
            results: results,
          };
        } catch (e) {
          // To prevent one search provider breaking all of search we have caught exceptions and logged it for later
          console.error(
            `Provider [${provider.name}] threw an exception, and has been ignored: `,
            e
          );
          return {
            provider: provider,
            results: [],
          };
        }
      })
      .filter(provider => provider.results.length);

    if (results.length > 0) {
      // Check if we only have one provider with search results
      const singleProvider = results[0].provider;
      if (results.length === 1 && singleProvider.onOnlyResult) {
        // If they declare an onOnlyResult handler, we give them an opportunity to present a custom whisper
        // please note that currently providers must do a second search in this situation
        const result = await singleProvider.onOnlyResult(query);
        if (result !== false) {
          // If they anything other than false we end search processing.
          return [];
        }
      }

      // If we have multiple search providers OR the search provider chooses not to handle being a single result provider
      // we present the search results in a common format
      const transformed = results.map<(WhisperComponent | Component)[]>(
        item => {
          return [
            {
              type: WhisperComponentType.Markdown,
              body: `## ${item.provider.name}`,
            },
            ...item.results,
          ];
        }
      );
      return flatMap(transformed); // Return list of result components as a flat list.
    }
    return [];
  }

  return {
    doSearch: doSearch,
  };
}
