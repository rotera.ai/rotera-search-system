/*
 * Copyright (C) 2021 Rotera LLC, All Rights Reserved
 *
 * This code is not to be shared or used without explicit written permission.
 * Written permission includes any contracts and other agreements detailing use
 * of source code.
 */

import {createSearchSystem} from '../src';
import {SearchProvider} from '../src/types/searchSystemTypes';
import {WhisperComponentType} from '@oliveai/ldk/dist/whisper';

afterEach(() => {
  jest.clearAllMocks();
});

function createTestSearchProvider(
  name: string,
  dataSet: string[],
  singleResultMode: 'DECLARED' | 'DECLARED_BUT_DENY' | 'UNDECLARED'
): SearchProvider {
  function getFilteredData(query: string) {
    return dataSet.filter(item =>
      item.toLowerCase().includes(query.toLowerCase())
    );
  }

  let singleResultProvider: jest.Mock | undefined;

  switch (singleResultMode) {
    case 'DECLARED':
      singleResultProvider = jest.fn();
      break;
    case 'DECLARED_BUT_DENY':
      singleResultProvider = jest.fn().mockResolvedValue(false);
      break;
    case 'UNDECLARED':
      singleResultProvider = undefined;
      break;
  }
  return {
    name: name,
    getComponentsForResults: query => {
      const filteredData = getFilteredData(query);
      return filteredData.map(item => {
        return {
          type: WhisperComponentType.Markdown,
          body: item,
        };
      });
    },
    onOnlyResult: singleResultProvider,
  };
}

describe('Search System', () => {
  const {doSearch} = createSearchSystem([
    createTestSearchProvider(
      'Greeting Planet',
      ['Hello', 'World', 'Hello World', 'Cat World'],
      'UNDECLARED'
    ),
    createTestSearchProvider(
      'Dog Names',
      ['Cody', 'Fuzzy', 'Cat'],
      'UNDECLARED'
    ),
    createTestSearchProvider(
      "Can't touch this",
      ['popular song'],
      'DECLARED_BUT_DENY'
    ),
    createTestSearchProvider(
      'I can show you the world',
      ['aladin'],
      'DECLARED'
    ),
    {
      name: 'Crashing',
      getComponentsForResults: query => {
        if (query === 'cat') {
          throw 'Something thrown';
        }
        return [];
      },
    },
  ]);
  it('Can get results from a single provider', async () => {
    const results = await doSearch('hello');
    expect(results).toMatchInlineSnapshot(`
      Array [
        Object {
          "body": "## Greeting Planet",
          "type": "markdown",
        },
        Object {
          "body": "Hello",
          "type": "markdown",
        },
        Object {
          "body": "Hello World",
          "type": "markdown",
        },
      ]
    `);
  });
  it('Can get results from a multiple providers and a crashing search provider is ignored', async () => {
    const results = await doSearch('cat');
    expect(results).toMatchInlineSnapshot(`
      Array [
        Object {
          "body": "## Greeting Planet",
          "type": "markdown",
        },
        Object {
          "body": "Cat World",
          "type": "markdown",
        },
        Object {
          "body": "## Dog Names",
          "type": "markdown",
        },
        Object {
          "body": "Cat",
          "type": "markdown",
        },
      ]
    `);
  });
  it('Can show results if provider declines single results', async () => {
    const results = await doSearch('popular song');
    expect(results).toMatchInlineSnapshot(`
      Array [
        Object {
          "body": "## Can't touch this",
          "type": "markdown",
        },
        Object {
          "body": "popular song",
          "type": "markdown",
        },
      ]
    `);
  });
  it('Does not show results if provider handles single results', async () => {
    const results = await doSearch('aladin');
    expect(results).toHaveLength(0);
  });

  it('Does not show results if no providers finds matche', async () => {
    const results = await doSearch('random text');
    expect(results).toHaveLength(0);
  });
});
