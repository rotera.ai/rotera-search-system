# Rotera Search System

A framework for generating search results from various different data sources

To create a search system you simply call:
`createSearchSystem(providers:  SearchProviders[])`

Search Providers
---
In the Rotera Search System, all search results come from Search Providers. 

    const contacts = ["Michael", "Keith"];
    const contactsSearchProvider: SearchProvider = {
        name: "Contacts",
        getComponentsForResults: query => { // <- Called for every query
          const filteredData = contacts.filter(contact => contact.toLowerCase().includes(query));
          return filteredData.map(contact => {
            return { // <- returns Olive Helps components
              type: WhisperComponentType.Markdown,
              body: contact,
            };
          });
        },
        onOnlyResult: (query) => { return false; }, // <- Called if the contact search provider is the only search provider with results
    }

The above is an example of a contacts search provider that returns all contacts who match a given search. For example, 
if a user searches for "mic" the above search provider would return a markdown component that says "Michael".

If a search system runs a query, and only one search provider has results, the search system will run the optional onOnlyResult
function provided by the search provider. This gives the search provider the opportunity to present its own, more detailed, whisper regarding the results.
Search providers can return `false` to tell the search system to present the normal search results whisper. 
Please note that currently providers must do a second search in this situation.

Search System functions
---

`doSearch(query: string)` - Use whenever you'd like to search for results. This function will automatically call the 
`getComponentsForResults` function on each search provider, and present a whisper showing each result under a bolded
label naming which search provider it came from. 

Running `doSearch('e')` using a search system configured with the above contactsSearchProvider would show a whisper formatted
like the following:

    **Contacts** <-- Name of the Provider
    Michael <-- The two results, formatted as markdown
    Keith 
